from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.views import View
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import ModelFormMixin, CreateView, DeletionMixin
from django.core.exceptions import ObjectDoesNotExist
from .models import *
from .forms import *

User = get_user_model()

def set_state(request, users):

    '''
    Set property that show is your blogger
    '''

    for user in users:
        try:
            blog = Blog.objects.get(blogger=user, subscriber=request.user)
            user.is_your_blogger = True
        except ObjectDoesNotExist: user.is_your_blogger = False
        yield user

class Index(View):

    http_method_names = ('get', 'post', 'head', 'options')
    template_name = 'index.html'

    def get(self, request):
        if request.user.is_anonymous: posts = []; bloggers = []
        else:
            blogs = Blog.objects.filter(subscriber=request.user)
            bloggers = set_state(self.request, (b.blogger for b in blogs))
            posts = Post.objects.filter(Q(owner=request.user) | \
            Q(owner__in=[blog.blogger_id for blog in blogs]))
        return render(request, self.template_name, locals())

    def post(self, request):
        # mark some post is read!
        post_id = request.POST.get('post_id')
        post = Post.objects.get(id=post_id)
        if request.user in post.viewers.all():
            return HttpResponseRedirect('/')
        post.viewers.add(request.user)
        post.save()
        return HttpResponseRedirect('/')

class Bloggers(ListView):

    http_method_names = ('get', 'head', 'options')
    template_name = "bloggers.html"
    model = User

    def get_success_url(self, **kwargs):
        return '/'

    def get_context_data(self, **kwargs):
        kwargs['bloggers'] = set_state(self.request, self.model.objects.all())
        return super(Bloggers, self).get_context_data(**kwargs)

class Blogger(DetailView):

    http_method_names = ('get', 'post', 'head', 'options')
    template_name = "blogger.html"
    model = User
    success_url = '/'

    def get_success_url(self, **kwargs):
        return '/'

    def get_context_data(self, **kwargs):
        user = kwargs.get('object')
        try:
            blog = Blog.objects.get(blogger=kwargs.get('object'), subscriber=self.request.user)
            user.is_your_blogger = True
        except ObjectDoesNotExist: user.is_your_blogger = False
        kwargs['blogger'] = user
        return super(Blogger, self).get_context_data(**kwargs)

    def post(self, request, pk):
        if request.POST.get('is_deleting', 0):
            try:
                Blog.objects.get(blogger=User.objects.get(id=pk), subscriber=request.user).delete()
            except ObjectDoesNotExist: pass
            return HttpResponseRedirect(self.success_url)
        try:
            Blog(blogger=User.objects.get(id=pk), subscriber=request.user).save()
        except ObjectDoesNotExist: pass
        except SubscribedToHimselfException: pass
        return HttpResponseRedirect(self.success_url)

    def delete(self, request, pk):
        try:
            Blog.objects.get(blogger=User.objects.get(id=pk), subscriber=request.user).delete()
        except ObjectDoesNotExist: pass
        return HttpResponseRedirect(self.success_url)

class Posts(CreateView):

    http_method_names = ('get', 'post', 'head', 'options')
    template_name = 'posts_form.html'
    model = Post
    success_url = '/posts/'
    form_class = PostForm

    def get_context_data(self, **kwargs):
        kwargs['posts'] = self.model.objects.all()
        return super(Posts, self).get_context_data(**kwargs)

    def form_valid(self, form):
        post = form.save(commit=False)
        post.owner = self.request.user
        self.object = post.save()
        return HttpResponseRedirect(self.success_url)

class PostDetail(DetailView):

    http_method_names = ('get', 'delete', 'head', 'options')
    template_name = 'post.html'
    model = Post

    def get_context_data(self, **kwargs):
        kwargs['post'] = kwargs['object']
        return super(Posts, self).get_context_data(**kwargs)
