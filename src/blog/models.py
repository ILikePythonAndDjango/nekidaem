from django.db import models
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.urls import reverse

User = get_user_model()

class SubscribedToHimselfException(Exception):
    pass

class Post(models.Model):

    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='owner_of_post')
    title = models.CharField(max_length=100)
    content = models.TextField()
    date = models.DateField(auto_now_add=True)
    viewers = models.ManyToManyField(User, blank=True, related_name='+')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.pk is None:
            super(Post, self).save(*args, **kwargs)
            subscribers = (b.subscriber for b in Blog.objects.filter(blogger=self.owner))
            for subscriber in subscribers:
                send_mail(
                    # subject
                    'New post from {author}'.format(author=self.owner),
                    # message
                    'Hello, {username}!\nContent:\n{content}\nyou can continue reading under the link {url}'.format(
                        username=subscriber.username,
                        content=self.content[:100],
                        url=self.get_absolute_url()
                    ),
                    # from
                    'testerfornekidaem@gmail.com',
                    # to
                    [subscriber.email],
                    fail_silently=False
                )
        else:
            super(Post, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.id})

    class Meta:
        ordering = ('-date', '-id')

class Blog(models.Model):

    blogger = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    subscriber = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')

    def __str__(self):
        return '{} subscribe on {}'.format(self.subscriber, self.blogger)

    def save(self, *args, **kwargs):
        if self.blogger_id == self.subscriber_id:
            raise SubscribedToHimselfException
            return
        try:
            super(Blog, self).save(*args, **kwargs)
            # catching UNIQUE constraint failed: blog_blog.blogger_id, blog_blog.subscriber_id
        except: pass
        finally: return

    class Meta:
        unique_together = (('blogger', 'subscriber'),)
