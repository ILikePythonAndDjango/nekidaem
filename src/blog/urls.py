from django.urls import path
from . import views

urlpatterns = [
    path('', views.Index.as_view()),
    path('bloggers/', views.Bloggers.as_view()),
    path('bloggers/<int:pk>/', views.Blogger.as_view()),
    path('posts/', views.Posts.as_view(), name='post-list'),
    path('posts/<int:pk>/', views.PostDetail.as_view(), name='post-detail')
]
