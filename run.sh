#!/bin/bash
rm -f ./src/local_settings.py
python3 ./src/manage.py migrate
gunicorn -c etc/gunicorn.py --pythonpath $(pwd)/src src.wsgi:application
